package com.sundaymobility.gochha.service.impl;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.QueryResultPage;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.sundaymobility.gochha.component.VideoManipulation;
import com.sundaymobility.gochha.data.entity.User;
import com.sundaymobility.gochha.data.entity.UserVideo;
import com.sundaymobility.gochha.data.exception.ResourceNotFoundException;
import com.sundaymobility.gochha.data.model.request.AllVideoRequest;
import com.sundaymobility.gochha.data.model.request.UserVideoRequest;
import com.sundaymobility.gochha.data.model.response.ApiResponse;
import com.sundaymobility.gochha.data.model.response.ApiResponseError;
import com.sundaymobility.gochha.data.model.response.UserVideoResponse;
import com.sundaymobility.gochha.repository.UserRepository;
import com.sundaymobility.gochha.repository.VideoRepository;
import com.sundaymobility.gochha.service.VideoService;
import com.sundaymobility.gochha.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class VideoServiceImpl implements VideoService {

    public static final Logger logger = LoggerFactory.getLogger(VideoService.class.getSimpleName());

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VideoRepository videoRepository;
    
    @Autowired
    private AmazonDynamoDB amazonDynamoDB;
    @Autowired
    private VideoManipulation videoManipulation;

    //1. stores user uploaded video info to the database
    //2. update accessUrl of the uploaded file in the database against original video
    //3. pass file for further video processing
    @Override
    public ApiResponse storeVideo(String userId, MultipartFile multipartFile) throws Exception {
        logger.info("userID {}", userId);
        Optional<User> byId = userRepository.findById(userId);
        if (byId.isEmpty())
            throw new ResourceNotFoundException(Constants.USER_NOT_FOUND);

        //throw bad request if there are more than a file
        if (Objects.isNull(multipartFile))
            return new ApiResponse.Builder<>().setError(new ApiResponseError.Builder().setCode(HttpStatus.BAD_REQUEST.value()).build()).build();

        String originalFileName = multipartFile.getOriginalFilename();
        String fileName = originalFileName.lastIndexOf('.') > originalFileName.lastIndexOf(File.separatorChar) ?
                originalFileName.substring(0, originalFileName.lastIndexOf('.')) : originalFileName;
        String sourceFileLocation = UriComponentsBuilder.fromPath("videos/" + userId + "/" + fileName + "/" + originalFileName).encode().build().toUriString();

        //store file info to db
        UserVideo userVideo = new UserVideo();
        userVideo.setId(userId);
        userVideo.setTitle(fileName);
        userVideo.setIsProcessed(false);
        UserVideo video = videoRepository.save(userVideo);
        logger.info("original file info stored to db with videoID => " + video.getId());

        //This is async => upload process runs in the background
        //upload original file to amazon s3 bucket
        byte[] bytes = multipartFile.getBytes();
        videoManipulation.uploadToAws(video.getVideoId(), multipartFile.getInputStream(), bytes, sourceFileLocation);

        //pass video for processing
        if (Objects.nonNull(video.getId())) {
            //convert video resolutions to different sizes
            videoManipulation.manipulateVideo(userId, video.getVideoId(), originalFileName, fileName, bytes);
        }

        return new ApiResponse.Builder<>().setSuccess(true).setData(null).build();
    }


    @Override
    public ApiResponse getAllVideos(AllVideoRequest userVideoRequest) {

        // added pagination for 15 records
        Pageable pageable = PageRequest.of(userVideoRequest.getPageNo(), 15);
        Page<UserVideo> userVideosList = videoRepository.findAllByIsProcessed(pageable, true);

        List<UserVideoResponse> listOfVideoResponse = new ArrayList<>();
        if (Objects.nonNull(userVideosList) && !userVideosList.isEmpty()) {
            for (UserVideo userVideo : userVideosList) {

                //add resolutions
                listOfVideoResponse.add(
                        new UserVideoResponse.UserVideoResponseBuilder()
                                .setId(userVideo.getId())
                                .setTitle(userVideo.getTitle())
                                .thumbnail(userVideo.getThumbnail())
                                .setHigh(getResolution(userVideo.getResolutions()))
                                .setMid(getResolution(userVideo.getResolutions()))
                                .setLow(getResolution(userVideo.getResolutions()))
                                .createUserVideoResponse()
                );
            }
        }
        return new ApiResponse.Builder<>().setSuccess(true).setData(listOfVideoResponse).build();
    }

    @Override
    public ApiResponse getAllVideosOfUser(String userId, UserVideoRequest userVideoRequest) {

        Pageable pageable = PageRequest.of(userVideoRequest.getPageNo(), 15);
        Page<UserVideo> videoPage = videoRepository.findByIdOrderByDateCreatedDesc(userId, pageable);
        
        
        
        ///
        DynamoDBMapper mapper  = new DynamoDBMapper(amazonDynamoDB);
        DynamoDBQueryExpression<UserVideo> userVideoExp = new DynamoDBQueryExpression<UserVideo>();
        userVideoExp.setKeyConditionExpression("#i = :userI AND #d = :dateC" );
        userVideoExp.addExpressionAttributeNamesEntry("#i","userId");
        userVideoExp.addExpressionAttributeNamesEntry("#d", "dateCreated");
        userVideoExp.setScanIndexForward(false);
        
        
        AttributeValue value = new AttributeValue();
        value.setBOOL(true);
        value.set
        userVideoExp.addExpressionAttributeValuesEntry(":isProcess", value)	;
        userVideoExp.setLimit(2);
        
        QueryResultPage<UserVideo> result = mapper.queryPage(UserVideo.class, userVideoExp);
        
        System.out.println(result.getResults());
        for(UserVideo v : result.getResults())
        {
        System.out.println(v.getTitle());
        }

        if (videoPage.isEmpty())
            return new ApiResponse.Builder<>().setError(new ApiResponseError.Builder().setCode(HttpStatus.NOT_FOUND.value()).build()).build();

        List<UserVideoResponse> allVideosOfUser = new ArrayList<>();
        for (UserVideo userVideo : videoPage) {
            allVideosOfUser.add(
                    new UserVideoResponse.UserVideoResponseBuilder()
                            .setId(userVideo.getId())
                            .setTitle(userVideo.getTitle())
                            .thumbnail(userVideo.getThumbnail())
                            .setHigh(getResolution(userVideo.getResolutions()))
                            .setMid(getResolution(userVideo.getResolutions()))
                            .setLow(getResolution(userVideo.getResolutions()))
                            .setDateCreated(userVideo.getDateCreated().getTime())
                            .createUserVideoResponse()
            );
        }
        return new ApiResponse.Builder<>()
                .setSuccess(true)
                .setData(allVideosOfUser)
                .build();
    }


    //looks the resolutions by the index and accordingly add it to the corresponding resolution object
    private UserVideo.Resolution getResolution(List<UserVideo.Resolution> convertedResolutions) {

        //TODO needs to change this logic of finding resolutions by the index
        //find if the list size is greater than x if yes then the next element is present
        //so we can perform the next operation to set the resolution
        if (Objects.nonNull(convertedResolutions)) {
            //high
            if (convertedResolutions.size() > 0) {
                if (Objects.nonNull(convertedResolutions.get(0))) {
                    UserVideo.Resolution highResolution = convertedResolutions.get(0);
                    if (Objects.nonNull(highResolution)) {
                        return highResolution;
                    }
                }
            }

            //mid
            if (convertedResolutions.size() > 1) {
                if (Objects.nonNull(convertedResolutions.get(1))) {
                    UserVideo.Resolution midResolution = convertedResolutions.get(1);
                    if (Objects.nonNull(midResolution)) {
                        return midResolution;
                    }
                }
            }

            //low
            if (convertedResolutions.size() > 2) {
                if (Objects.nonNull(convertedResolutions.get(2))) {
                    UserVideo.Resolution lowResolution = convertedResolutions.get(2);
                    if (Objects.nonNull(lowResolution)) {
                        return lowResolution;
                    }
                }
            }
        }
        return null;
    }

}
