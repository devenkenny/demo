package com.sundaymobility.gochha.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sundaymobility.gochha.component.VideoManipulation;
import com.sundaymobility.gochha.data.entity.UserVideo;
import com.sundaymobility.gochha.data.model.request.UserVideoRequest;
import com.sundaymobility.gochha.data.model.response.ApiResponse;
import com.sundaymobility.gochha.data.model.response.ApiResponseError;
import com.sundaymobility.gochha.data.model.response.UserVideoResponse;
import com.sundaymobility.gochha.repository.VideoRepository;
import com.sundaymobility.gochha.security.JwtTokenUtil;
import com.sundaymobility.gochha.service.VideoService;

@Service
public class VideoServiceImpl implements VideoService {
	
    @Autowired
    private VideoRepository repository;
    
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    
    
    public static final Logger logger = LoggerFactory.getLogger(VideoService.class.getSimpleName());

    private final VideoManipulation videoManipulation;

    @Autowired
    public VideoServiceImpl(VideoManipulation videoManipulation) {
        this.videoManipulation = videoManipulation;
    }


    @Override
    public ApiResponse storeVideo(MultipartFile multipartFile) throws Exception {

        if (Objects.isNull(multipartFile))
            return new ApiResponse.Builder<>().setError(new ApiResponseError.Builder().setCode(HttpStatus.BAD_REQUEST.value()).build()).build();

        //convert video resolutions to different sizes
        videoManipulation.manipulateVideo(multipartFile);

        return new ApiResponse.Builder<>().setSuccess(true).setData(null).build();
    }


	@Override
	public ApiResponse getAllVideos(UserVideoRequest userVideoRequest,String authToken) {
		
		
		if(!jwtTokenUtil.validateToken(authToken))
		{
			return new ApiResponse.Builder<>().setSuccess(false).setData("Jwt token not valid or expired").build();	
		}
		// added pagination for 15 records 
		Pageable pageable = PageRequest.of(userVideoRequest.getPageNo(),15);
		
		Page<UserVideo> userVideosList = repository.findAll(pageable);

		List<UserVideoResponse> listOfVideoResponse = new ArrayList<>();
		
		if(Objects.nonNull(userVideosList) && !userVideosList.isEmpty())
		{
		for(UserVideo userVideo :userVideosList)
		{
			UserVideoResponse userVideoResponse = new UserVideoResponse();
			//userVideoResponse.setId(userVideo.getId());
			userVideoResponse.setTitle(userVideo.getTitle());
			userVideoResponse.setUrl(userVideo.getUrl());
			listOfVideoResponse.add(userVideoResponse);
		}
		}
		return new ApiResponse.Builder<>().setSuccess(true).setData(listOfVideoResponse).build();
	}


	@Override
	public ApiResponse getVideoByUserId(UserVideoRequest userVideoRequest,String authToken) {
		
		if(!jwtTokenUtil.validateToken(authToken))
		{
			return new ApiResponse.Builder<>().setSuccess(false).setData("Jwt token not valid or expired").build();	
		}
		
	UserVideo userVideo = 	repository.findByUserId(userVideoRequest.getUserId());
	if(Objects.nonNull(userVideo))
	{
		UserVideoResponse userVideoResponse = new UserVideoResponse();
		//userVideoResponse.setId(userVideo.getId());
		userVideoResponse.setTitle(userVideo.getTitle());
		userVideoResponse.setUrl(userVideo.getUrl());
		return new ApiResponse.Builder<>().setSuccess(true).setData(userVideoResponse).build();
	}
	else
	{
		return new ApiResponse.Builder<>().setSuccess(false).setData("No data found").build();	
	}
	}


	@Override
	public ApiResponse addVideo() {
		UserVideo userVideo = new UserVideo();
		userVideo.setTitle("title");
		userVideo.setUrl("uuurl");
		//userVideo.setUserId("100");
		repository.save(userVideo);
		return null;
	}



}
