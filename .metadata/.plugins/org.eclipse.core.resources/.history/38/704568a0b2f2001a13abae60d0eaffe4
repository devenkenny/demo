package com.sundaymobility.gochha.component;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.sundaymobility.gochha.config.AmazonClientConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;

public class AmazonS3Client {

    private AmazonS3 amazonS3Client;
    private final AmazonClientConfiguration amazonClientConfiguration;

    @Autowired
    public AmazonS3Client(AmazonClientConfiguration amazonClientConfiguration) {
        this.amazonClientConfiguration = amazonClientConfiguration;
    }

    @PostConstruct
    private void init() {
        AWSCredentials awsCredentials = new BasicAWSCredentials(amazonClientConfiguration.getAccessKey(), amazonClientConfiguration.getSecretKey());
        amazonS3Client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .withRegion(Regions.AP_SOUTH_1)
                .build();
    }

    public PutObjectResult upload(MultipartFile multipartFile, String key) throws Exception {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(multipartFile.getSize());
        PutObjectRequest putObjectRequest =
                new PutObjectRequest(amazonClientConfiguration.getBucketName(), key,
                        multipartFile.getInputStream(), objectMetadata);
        return amazonS3Client.putObject(putObjectRequest);
    }

    public S3ObjectInputStream view(String key) {
        S3Object s3Object = amazonS3Client.getObject(amazonClientConfiguration.getBucketName(), key);
        S3ObjectInputStream stream = s3Object.getObjectContent();
        return stream;
    }

}
