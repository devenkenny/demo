package com.sundaymobility.gochha.controller;

import com.sundaymobility.gochha.data.model.request.AllVideoRequest;
import com.sundaymobility.gochha.data.model.request.UserVideoRequest;
import com.sundaymobility.gochha.data.model.response.ApiResponse;
import com.sundaymobility.gochha.data.model.response.ApiResponseError;
import com.sundaymobility.gochha.security.JwtTokenUtil;
import com.sundaymobility.gochha.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("video")
public class VideoResource {

    @Autowired
    private VideoService videoService;

    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity uploadVideo(@RequestHeader("Authorization") String authenticationToken, @RequestPart("file") MultipartFile multipartFile) throws Exception {
        //Todo: Need to remove later
        if (!JwtTokenUtil.validateToken(authenticationToken))
            return ResponseEntity.ok(new ApiResponse.Builder<>()
                    .setSuccess(false)
                    .setError(new ApiResponseError.Builder()
                            .setCode(401)
                            .setMessage("Unauthorized").build()).build());
        return ResponseEntity.ok(videoService.storeVideo(JwtTokenUtil.getIdFromToken(authenticationToken), multipartFile));
    }

    @PostMapping(value = "/all")
    public ResponseEntity getAllVideos(@RequestHeader("Authorization") String authenticationToken, @RequestBody AllVideoRequest allVideoRequest) {
        //Todo: Need to remove later
        if (!JwtTokenUtil.validateToken(authenticationToken))
            return ResponseEntity.ok(new ApiResponse.Builder<>()
                    .setSuccess(false)
                    .setError(new ApiResponseError.Builder()
                            .setCode(401)
                            .setMessage("Unauthorized").build()).build());
        return ResponseEntity.ok(videoService.getAllVideos(allVideoRequest));
    }

    @PostMapping(value = "/user/all")
    public ResponseEntity getAllUserVideos(@RequestHeader("Authorization") String authenticationToken, @RequestBody UserVideoRequest userVideoRequest) {
        //Todo: Need to remove later
        if (!JwtTokenUtil.validateToken(authenticationToken))
            return ResponseEntity.ok(new ApiResponse.Builder<>()
                    .setSuccess(false)
                    .setError(new ApiResponseError.Builder()
                            .setCode(401)
                            .setMessage("Unauthorized").build()).build());
        return ResponseEntity.ok(videoService.getAllVideosOfUser(JwtTokenUtil.getIdFromToken(authenticationToken), userVideoRequest));
    }
}
